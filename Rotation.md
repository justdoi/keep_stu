##opencv的getRotationMatrix2D函数获取旋转矩阵
####函数原型
getRotationMatrix2D(Point2f center,double angle,double scale )<br>
center：旋转中心坐标，是一个元组参数(col, row)

angle：旋转角度，旋转方向，正号为逆时针，负号为顺时针

scale：旋转后图像相比原来的缩放比例，1为等比例缩放	
####得到的矩阵介绍
![M1.png](M1.png)<br>
其中<br>
![M2.png](M2.png)<br>
矩阵两行的最后两个数是旋转中心，分别为旋转中心的x和y<br>
在旋转过程中为了旋转后不遮挡旋转后的原图，需要对原图进行长宽的变换，要注意旋转中心在变换后图像上的映射坐标要加上差值。


