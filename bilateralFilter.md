##双边滤波
非线性滤波，允许高频噪点通过。<br>
双边滤波就是既考虑xy坐标维度的高斯卷积核，也考虑颜色空间BGR维度的高斯卷积核
##具体代码（自己理解）
	img=cv2.imread("lenaNoise.png")
	#双边滤波（非线性滤波，能在出去低频噪音点的同时保留边缘信息，但无法出去高频噪音点）（高频噪音点就是像素点的值与周围像素点的值差距较大）
	filter6=cv2.bilateralFilter(img,9,20,75)        
	#第二个参数是卷积领域的直径，第三个参数为高斯颜色空间的核标准差，表示多大的像素差值会被计算
	#第三个参数高斯颜色空间核标准差表示核中与中心像素值差值很大的位置对中心的影响，该值越大，影响越小
	#第四个参数为高斯坐标空间函数标准差，当第二个参数为非正数就根据这个差值来计算卷积内核大小（即高斯滤波的公式），该值越大，模糊效果越强，当第二个参数为正数，该值不起作用
	# cv_show("filter6",filter6)
	filters=np.hstack((filter6,img))
	cv_show("filters",filters)
具体参考：[https://blog.csdn.net/keith_bb/article/details/54427779](https://blog.csdn.net/keith_bb/article/details/54427779)
