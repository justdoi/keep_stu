##normalize归一化函数处理
normalize (InputArray  src, InputOutputArray  dst, double  alpha=1, double  beta=0, int  norm\_type=NORM\_L2, int  dtype=-1, InputArray  mask=noArray()  )
#
	src –- input array.
	dst –- output array of the same size as src .
	alpha -– norm value to normalize to or the lower range boundary in case of the range normalization.
	beta –- upper range boundary in case of the range normalization; it is not used for the norm normalization.
	normType -– normalization type (see the details below).
	dtype –- when negative, the output array has the same type as src; otherwise, it has the same number of channels as src and the depth =CV_MAT_DEPTH(dtype).
	mask -– optional operation mask
##值归一化和范围归一化（normalize and range normalization）
值归一化是指归一化到[0-x]；<br>
范围归一化是指归一化到一个范围内[x,y]；<br>
##归一化函数的norm\_type
#
	NORM_L1:以数组中的和为1(x/sum)
	NORM_L2:不懂
	NORM_INF:以数组中的最大值为1(x/max)
	NORM_MINMAX:将数组的值线性映射到下限到上限
具体参考：[https://blog.csdn.net/kuweicai/article/details/78988886?utm_term=%E5%BD%92%E4%B8%80%E5%8C%96%E5%87%BD%E6%95%B0normalize%E8%AF%A6%E8%A7%A3&utm_medium=distribute.pc_aggpage_search_result.none-task-blog-2~all~sobaiduweb~default-1-78988886&spm=3001.4430](https://blog.csdn.net/kuweicai/article/details/78988886?utm_term=%E5%BD%92%E4%B8%80%E5%8C%96%E5%87%BD%E6%95%B0normalize%E8%AF%A6%E8%A7%A3&utm_medium=distribute.pc_aggpage_search_result.none-task-blog-2~all~sobaiduweb~default-1-78988886&spm=3001.4430)