##On_Mouse响应函数（鼠标响应）
####固定传参格式
#	
	void OnMouseAction(int event,int x,int y,int flags,void *ustc); 
	
####event事件种类
	Event:
	#define CV_EVENT_MOUSEMOVE 0             //滑动
	#define CV_EVENT_LBUTTONDOWN 1           //左键点击
	#define CV_EVENT_RBUTTONDOWN 2           //右键点击
	#define CV_EVENT_MBUTTONDOWN 3           //中键点击
	#define CV_EVENT_LBUTTONUP 4             //左键放开
	#define CV_EVENT_RBUTTONUP 5             //右键放开
	#define CV_EVENT_MBUTTONUP 6             //中键放开
	#define CV_EVENT_LBUTTONDBLCLK 7         //左键双击
	#define CV_EVENT_RBUTTONDBLCLK 8         //右键双击
	#define CV_EVENT_MBUTTONDBLCLK 9         //中键双击
####flags事件种类
	flags:
	#define CV_EVENT_FLAG_LBUTTON 1       //左鍵拖曳
	#define CV_EVENT_FLAG_RBUTTON 2       //右鍵拖曳
	#define CV_EVENT_FLAG_MBUTTON 4       //中鍵拖曳
	#define CV_EVENT_FLAG_CTRLKEY 8       //(8~15)按Ctrl不放事件
	#define CV_EVENT_FLAG_SHIFTKEY 16     //(16~31)按Shift不放事件
	#define CV_EVENT_FLAG_ALTKEY 32       //(32~39)按Alt不放事件
具体参考：[https://blog.csdn.net/dcrmg/article/details/52027847](https://blog.csdn.net/dcrmg/article/details/52027847)