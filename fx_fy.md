##什么是反向投影
####就是根据反射来获取到目标对象，现实生活中参考回声定位。
##
##opencv中的反向投影
####就是根据一个要查找的目标对象的直方图来获取到这个目标对象在某张图上的大概位置，相似位置。
##
##实现原理
        (1)例如灰度图像如下 ：
               Image=

                      0    1    2    3
                      4    5    6    7
                      8    9   10   11
                      8    9   14   15
        (2)该灰度图的直方图为（bin指定的区间为[0,3)，[4,7)，[8,11)，[12,16)）
                Histogram=        4    4    6    2

 

        (3)反向投影图
                Back_Projection=
                      4    4    4    4
                      4    4    4    4
                      6    6    6    6
                      6    6    2    2
####根据以上原理，当反向投影得到的图像某一像素点的值越大，那么就说明该值所在区间（bin）的占比越多，而opencv中封装了calcBackProject（）函数可以将直方图在原图中比对，最终得到反向投影图。根据反向投影图各像素点的值大小确定出该区域是否和目标对象相似度（或者说是目标对象在该区域的出现概率）。
具体参考：[https://blog.csdn.net/shuiyixin/article/details/80331839](https://blog.csdn.net/shuiyixin/article/details/80331839)