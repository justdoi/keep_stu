##傅里叶变换（有两种做法，numpy和opencv）
#####傅里叶变换<br>
	opencv:
	cv2.dft(img,cv2.DFT_COMPLEX_OUTPUT)#傅里叶变换函数，获取图像的频谱图
	参数说明: img表示输入的图片（要转成float32格式）， cv2.DFT\_COMPLEX_OUTPUT表示进行傅里叶变化的方法，返回值是一张有实部和虚部的双通道图像

	np.fft.fftshift(img)#将频谱图移到中心，即将低频部分从左上角移到图像中央，频谱越亮的位置频率越低

	cv2.magnitude(x,y)#将实部和虚部按照sqrt(x^2+y^2)运算化为单通道数
	

	numpy:
	np.fft.fft2(img)#不用转成float32格式传入，返回值为一个由复数组成的频谱图

	np.fft.fftshift(img)#不用多说

	20*np.log(np.abs(img))#将复数化为0-255范围之间的数，以显示频谱图

#####傅里叶逆变换
	opencv:
	np.fft.ifftshift(img)#将频谱图移回左上角，频谱越亮的位置频率越低
	
	cv2.idft(img)#傅里叶逆变换函数，返回值是一张有实部和虚部的双通道图像

	cv2.magnitude(x,y)#将实部和虚部按照sqrt(x^2+y^2)运算化为单通道数,且图像上的值在0-255之间，以展示最后滤波操作后的结果（要用plt展示，因为最后结果图像的元素数据类型为float32）
	

	numpy:
	np.fft.ifftshift(img)#不用多说
	
	np.fft.ifft2(img)#返回值为一个由复数组成的频谱图

	np.abs(img)#将复数化为0-255范围之间的数，以显示最后的结果（要用plt展示，最后元素数据类型为float64）
#####高通滤波器
保留频谱图中高频部分，去除低频部分就是高通滤波器，在图像中，根据傅里叶变换，像素值变化越剧烈，频率越高，对应在频谱图中越暗。主要用于获取边缘信息。源码实现：
#
	#高通滤波器
	def highgussi(img,size):
	    w,h=img.shape[:2]
	    img[int(w/2)-size:int(w/2)+size,int(h/2)-size:int(h/2)+size]=0
	    return img
#####低通滤波器
保留频谱中的低频部分，去除高频部分就是低通滤波器。低频部分对应频谱图中较亮的部分。源码实现：
#
	#低通滤波器
	def lowgussi(img,size):
	    w,h=img.shape[:2]
	    mask=np.zeros(img.shape,np.uint8)
	    mask[int(w/2)-size:int(w/2)+size,int(h/2)-size:int(h/2)+size]=1
	    img=img*mask
	    return img